﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Guesthouse.Library.Common.Core.Application.Wrappers;
using Guesthouse.ServiceExample.Application.Interfaces.Repositories;
using Guesthouse.ServiceExample.Domain.Entities;
using MediatR;

namespace Guesthouse.ServiceExample.Application.Features.Positions.Commands.CreatePosition;

public class CreatePositionCommand : IRequest<Response<int>>
{
    public string PositionTitle { get; set; }
    public string PositionNumber { get; set; }
    public string PositionDescription { get; set; }
    public decimal PositionSalary { get; set; }
}

public class CreatePositionCommandHandler : IRequestHandler<CreatePositionCommand, Response<int>>
{
    private readonly IMapper _mapper;
    private readonly IPositionRepository _positionRepository;

    public CreatePositionCommandHandler(IPositionRepository positionRepository, IMapper mapper)
    {
        _positionRepository = positionRepository;
        _mapper = mapper;
    }

    public async Task<Response<int>> Handle(CreatePositionCommand request, CancellationToken cancellationToken)
    {
        var position = _mapper.Map<Position>(request);
        await _positionRepository.Add(position, cancellationToken);
        return new Response<int>(position.Id);
    }
}