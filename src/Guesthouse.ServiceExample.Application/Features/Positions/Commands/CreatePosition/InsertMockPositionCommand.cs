﻿using System.Threading;
using System.Threading.Tasks;
using Guesthouse.Library.Common.Core.Application.Wrappers;
using Guesthouse.ServiceExample.Application.Interfaces.Repositories;
using MediatR;

namespace Guesthouse.ServiceExample.Application.Features.Positions.Commands.CreatePosition;

public class InsertMockPositionCommand : IRequest<Response<int>>
{
    public int RowCount { get; set; }
}

public class SeedPositionCommandHandler : IRequestHandler<InsertMockPositionCommand, Response<int>>
{
    private readonly IPositionRepository _positionRepository;

    public SeedPositionCommandHandler(IPositionRepository positionRepository)
    {
        _positionRepository = positionRepository;
    }

    public async Task<Response<int>> Handle(InsertMockPositionCommand request, CancellationToken cancellationToken)
    {
        await _positionRepository.SeedDataAsync(request.RowCount, cancellationToken);
        return new Response<int>(request.RowCount);
    }
}