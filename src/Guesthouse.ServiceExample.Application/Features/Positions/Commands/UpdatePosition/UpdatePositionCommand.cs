﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Guesthouse.Library.Common.Core.Application.Exceptions;
using Guesthouse.Library.Common.Core.Application.Wrappers;
using Guesthouse.ServiceExample.Application.Interfaces.Repositories;
using MediatR;

namespace Guesthouse.ServiceExample.Application.Features.Positions.Commands.UpdatePosition;

public class UpdatePositionCommand : IRequest<Response<int>>
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public decimal Salary { get; set; }

    public class UpdatePositionCommandHandler : IRequestHandler<UpdatePositionCommand, Response<int>>
    {
        private readonly IPositionRepository _positionRepository;

        public UpdatePositionCommandHandler(IPositionRepository positionRepository)
        {
            _positionRepository = positionRepository;
        }

        public async Task<Response<int>> Handle(UpdatePositionCommand command, CancellationToken cancellationToken)
        {
            var position = await _positionRepository.GetById(command.Id, cancellationToken);

            if (position == null)
            {
                throw new ApiException("Position Not Found.");
            }

            position.PositionTitle = command.Title;
            position.PositionSalary = command.Salary;
            position.PositionDescription = command.Description;
            await _positionRepository.Update(position, cancellationToken);
            return new Response<int>(position.Id);
        }
    }
}