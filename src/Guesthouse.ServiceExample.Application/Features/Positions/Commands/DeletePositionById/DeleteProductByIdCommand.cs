﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Guesthouse.Library.Common.Core.Application.Exceptions;
using Guesthouse.Library.Common.Core.Application.Wrappers;
using Guesthouse.ServiceExample.Application.Interfaces.Repositories;
using MediatR;

namespace Guesthouse.ServiceExample.Application.Features.Positions.Commands.DeletePositionById;

public class DeletePositionByIdCommand : IRequest<Response<int>>
{
    public Guid Id { get; set; }

    public class DeletePositionByIdCommandHandler : IRequestHandler<DeletePositionByIdCommand, Response<int>>
    {
        private readonly IPositionRepository _positionRepository;

        public DeletePositionByIdCommandHandler(IPositionRepository positionRepository)
        {
            _positionRepository = positionRepository;
        }

        public async Task<Response<int>> Handle(DeletePositionByIdCommand command, CancellationToken cancellationToken)
        {
            var position = await _positionRepository.GetById(command.Id, cancellationToken);
            if (position == null) throw new ApiException("Position Not Found.");
            await _positionRepository.Delete(position, cancellationToken);
            return new Response<int>(position.Id);
        }
    }
}