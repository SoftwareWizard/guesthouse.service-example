﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Guesthouse.Library.Common.Core.Application.Exceptions;
using Guesthouse.Library.Common.Core.Application.Wrappers;
using Guesthouse.ServiceExample.Application.Interfaces.Repositories;
using Guesthouse.ServiceExample.Domain.Entities;
using MediatR;

namespace Guesthouse.ServiceExample.Application.Features.Positions.Queries.GetPositionById;

public class GetPositionByIdQuery : IRequest<Response<Position>>
{
    public Guid Id { get; set; }

    public class GetPositionByIdQueryHandler : IRequestHandler<GetPositionByIdQuery, Response<Position>>
    {
        private readonly IPositionRepository _positionRepository;

        public GetPositionByIdQueryHandler(IPositionRepository positionRepository)
        {
            _positionRepository = positionRepository;
        }

        public async Task<Response<Position>> Handle(GetPositionByIdQuery query, CancellationToken cancellationToken)
        {
            var position = await _positionRepository.GetById(query.Id, cancellationToken);
            if (position == null) throw new ApiException("Position Not Found.");
            return new Response<Position>(position);
        }
    }
}