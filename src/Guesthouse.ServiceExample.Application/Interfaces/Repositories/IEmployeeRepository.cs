﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Guesthouse.Library.Common.Core.Application.Interfaces;
using Guesthouse.Library.Common.Core.Application.Parameters;
using Guesthouse.ServiceExample.Application.Features.Employees.Queries.GetEmployees;
using Guesthouse.ServiceExample.Domain.Entities;

namespace Guesthouse.ServiceExample.Application.Interfaces.Repositories;

public interface IEmployeeRepository : IRepository<Employee, Guid>
{
    Task<(IEnumerable<Entity> data, RecordsCount recordsCount)> GetPagedEmployeeReponseAsync(
        GetEmployeesQuery requestParameter);
}