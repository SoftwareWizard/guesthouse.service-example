﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Guesthouse.Library.Common.Core.Application.Interfaces;
using Guesthouse.Library.Common.Core.Application.Parameters;
using Guesthouse.ServiceExample.Application.Features.Positions.Queries.GetPositions;
using Guesthouse.ServiceExample.Domain.Entities;

namespace Guesthouse.ServiceExample.Application.Interfaces.Repositories;

public interface IPositionRepository : IRepository<Position, Guid>
{
    Task<bool> IsUniquePositionNumberAsync(string positionNumber);

    Task<bool> SeedDataAsync(int rowCount, CancellationToken cancellationToken);

    Task<(IEnumerable<Entity> data, RecordsCount recordsCount)> GetPagedPositionReponseAsync(
        GetPositionsQuery requestParameters);
}