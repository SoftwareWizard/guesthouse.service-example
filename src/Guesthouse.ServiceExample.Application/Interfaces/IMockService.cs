﻿using System.Collections.Generic;
using Guesthouse.ServiceExample.Domain.Entities;

namespace Guesthouse.ServiceExample.Application.Interfaces;

public interface IMockService
{
    List<Position> GetPositions(int rowCount);

    List<Employee> GetEmployees(int rowCount);

    List<Position> SeedPositions(int rowCount);
}