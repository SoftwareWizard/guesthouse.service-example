﻿using System;

namespace Guesthouse.ServiceExample.Application.Interfaces;

public interface IDateTimeService
{
    DateTime NowUtc { get; }
}