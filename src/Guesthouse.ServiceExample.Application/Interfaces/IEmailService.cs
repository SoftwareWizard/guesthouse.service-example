﻿using System.Threading.Tasks;
using Guesthouse.ServiceExample.Application.DTOs.Email;

namespace Guesthouse.ServiceExample.Application.Interfaces;

public interface IEmailService
{
    Task SendAsync(EmailRequest request);
}