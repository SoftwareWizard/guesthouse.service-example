﻿namespace Guesthouse.ServiceExample.Application.Enums;

public enum Roles
{
    SuperAdmin,
    HRAdmin,
    Manager,
    Employee
}