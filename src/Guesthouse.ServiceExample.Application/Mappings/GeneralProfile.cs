﻿using AutoMapper;
using Guesthouse.ServiceExample.Application.Features.Employees.Queries.GetEmployees;
using Guesthouse.ServiceExample.Application.Features.Positions.Commands.CreatePosition;
using Guesthouse.ServiceExample.Application.Features.Positions.Queries.GetPositions;
using Guesthouse.ServiceExample.Domain.Entities;

namespace Guesthouse.ServiceExample.Application.Mappings;

public class GeneralProfile : Profile
{
    public GeneralProfile()
    {
        CreateMap<Position, GetPositionsViewModel>().ReverseMap();
        CreateMap<Employee, GetEmployeesViewModel>().ReverseMap();
        CreateMap<CreatePositionCommand, Position>();
    }
}