﻿using System.Reflection;
using FluentValidation;
using Guesthouse.Library.Common.Core.Application.Extensions;
using Guesthouse.ServiceExample.Application.Helpers;
using Guesthouse.ServiceExample.Application.Interfaces;
using Guesthouse.ServiceExample.Domain.Entities;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Guesthouse.ServiceExample.Application;

public static class ServiceExtensions
{
    public static void AddApplicationLayer(this IServiceCollection services)
    {
        services.AddApplicationLayerFromLibrary();

        services.AddAutoMapper(Assembly.GetExecutingAssembly());
        services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
        services.AddMediatR(Assembly.GetExecutingAssembly());

        services.AddScoped<IDataShapeHelper<Position>, DataShapeHelper<Position>>();
        services.AddScoped<IDataShapeHelper<Employee>, DataShapeHelper<Employee>>();
        services.AddScoped<IModelHelper, ModelHelper>();
        //services.AddScoped<IMockData, MockData>();
    }
}