﻿namespace Guesthouse.ServiceExample.Application.Parameters;

public class PagingParameter
{
    private const int maxPageSize = 200;
    private int _pageSize = 10;
    public int PageNumber { get; set; } = 1;

    public int PageSize
    {
        get => _pageSize;
        set => _pageSize = value > maxPageSize ? maxPageSize : value;
    }
}