﻿using Guesthouse.ServiceExample.Application.Interfaces;
using Guesthouse.ServiceExample.Domain.Settings;
using Guesthouse.ServiceExample.Infrastructure.Shared.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Guesthouse.ServiceExample.Infrastructure.Shared;

public static class ServiceRegistration
{
    public static void AddSharedInfrastructure(this IServiceCollection services, IConfiguration _config)
    {
        services.Configure<MailSetting>(_config.GetSection("Mail"));
        services.AddTransient<IDateTimeService, DateTimeService>();
        services.AddTransient<IEmailService, EmailService>();
        services.AddTransient<IMockService, MockService>();
    }
}