﻿using System;
using System.Threading.Tasks;
using Guesthouse.Library.Common.Core.Application.Exceptions;
using Guesthouse.ServiceExample.Application.DTOs.Email;
using Guesthouse.ServiceExample.Application.Interfaces;
using Guesthouse.ServiceExample.Domain.Settings;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;

namespace Guesthouse.ServiceExample.Infrastructure.Shared.Services;

public class EmailService : IEmailService
{
    public EmailService(IOptions<MailSetting> mailSettings, ILogger<EmailService> logger)
    {
        MailSetting = mailSettings.Value;
        _logger = logger;
    }

    public MailSetting MailSetting { get; }
    public ILogger<EmailService> _logger { get; }

    public async Task SendAsync(EmailRequest request)
    {
        try
        {
            // create message
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(request.From ?? MailSetting.EmailFrom);
            email.To.Add(MailboxAddress.Parse(request.To));
            email.Subject = request.Subject;
            var builder = new BodyBuilder();
            builder.HtmlBody = request.Body;
            email.Body = builder.ToMessageBody();
            using var smtp = new SmtpClient();
            smtp.Connect(MailSetting.SmtpHost, MailSetting.SmtpPort, SecureSocketOptions.StartTls);
            smtp.Authenticate(MailSetting.SmtpUser, MailSetting.SmtpPass);
            await smtp.SendAsync(email);
            smtp.Disconnect(true);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex.Message, ex);
            throw new ApiException(ex.Message);
        }
    }
}