﻿using System;
using Guesthouse.ServiceExample.Application.Interfaces;

namespace Guesthouse.ServiceExample.Infrastructure.Shared.Services;

public class DateTimeService : IDateTimeService
{
    public DateTime NowUtc => DateTime.UtcNow;
}