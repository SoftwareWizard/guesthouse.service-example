﻿using System.Collections.Generic;
using Guesthouse.ServiceExample.Application.Interfaces;
using Guesthouse.ServiceExample.Domain.Entities;
using Guesthouse.ServiceExample.Infrastructure.Shared.Mock;

namespace Guesthouse.ServiceExample.Infrastructure.Shared.Services;

public class MockService : IMockService
{
    public List<Position> GetPositions(int rowCount)
    {
        var positionFaker = new PositionInsertBogusConfig();
        return positionFaker.Generate(rowCount);
    }

    public List<Employee> GetEmployees(int rowCount)
    {
        var positionFaker = new EmployeeBogusConfig();
        return positionFaker.Generate(rowCount);
    }

    public List<Position> SeedPositions(int rowCount)
    {
        var seedPositionFaker = new PositionSeedBogusConfig();
        return seedPositionFaker.Generate(rowCount);
    }
}