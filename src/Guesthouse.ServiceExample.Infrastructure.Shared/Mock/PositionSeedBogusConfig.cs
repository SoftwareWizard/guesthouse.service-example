﻿using System;
using AutoBogus;
using Bogus;
using Guesthouse.ServiceExample.Domain.Entities;

namespace Guesthouse.ServiceExample.Infrastructure.Shared.Mock;

public class PositionSeedBogusConfig : AutoFaker<Position>
{
    public PositionSeedBogusConfig()
    {
        Randomizer.Seed = new Random(8675309);
        var id = 1;
        RuleFor(o => o.Id, f => id++);
        RuleFor(o => o.PositionTitle, f => f.Name.JobTitle());
        RuleFor(o => o.PositionNumber, f => f.Commerce.Ean13());
        RuleFor(o => o.PositionDescription, f => f.Name.JobDescriptor());
        RuleFor(o => o.Created, f => f.Date.Past());
        RuleFor(o => o.CreatedBy, f => Guid.NewGuid());
        RuleFor(o => o.LastModified, f => f.Date.Recent());
        RuleFor(o => o.LastModifiedBy, f => Guid.NewGuid());
        RuleFor(o => o.PositionSalary, f => f.Finance.Amount());
    }
}