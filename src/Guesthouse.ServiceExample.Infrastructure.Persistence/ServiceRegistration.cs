﻿using System;
using Guesthouse.Library.Common.Core.Application.Interfaces;
using Guesthouse.Library.Common.Infrastructure.Persistence;
using Guesthouse.ServiceExample.Application.Interfaces.Repositories;
using Guesthouse.ServiceExample.Domain.Entities;
using Guesthouse.ServiceExample.Infrastructure.Persistence.Contexts;
using Guesthouse.ServiceExample.Infrastructure.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Guesthouse.ServiceExample.Infrastructure.Persistence;

public static class ServiceRegistration
{
    public static void AddPersistenceInfrastructure(this IServiceCollection services, IConfiguration configuration)
    {
        if (configuration.GetValue<bool>("UseInMemoryDatabase"))
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseInMemoryDatabase("ApplicationDb"));
        else
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));

        #region Repositories

        services.AddTransient(typeof(IRepository<Position, Guid>), typeof(Repository<Position, Guid, ApplicationDbContext>));
        services.AddTransient(typeof(IRepository<Employee, Guid>), typeof(Repository<Employee, Guid, ApplicationDbContext>));
        services.AddTransient<IPositionRepository, PositionRepository>();
        services.AddTransient<IEmployeeRepository, EmployeeRepository>();

        #endregion Repositories
    }
}