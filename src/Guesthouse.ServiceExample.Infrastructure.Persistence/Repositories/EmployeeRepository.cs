﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Guesthouse.Library.Common.Core.Application.Parameters;
using Guesthouse.Library.Common.Infrastructure.Persistence;
using Guesthouse.ServiceExample.Application.Features.Employees.Queries.GetEmployees;
using Guesthouse.ServiceExample.Application.Interfaces;
using Guesthouse.ServiceExample.Application.Interfaces.Repositories;
using Guesthouse.ServiceExample.Domain.Entities;
using Guesthouse.ServiceExample.Infrastructure.Persistence.Contexts;
using LinqKit;

namespace Guesthouse.ServiceExample.Infrastructure.Persistence.Repositories;

public class EmployeeRepository : Repository<Employee, Guid, ApplicationDbContext>, IEmployeeRepository
{
    private readonly IMockService _mockData;
    private readonly IDataShapeHelper<Employee> _dataShaper;

    public EmployeeRepository(ApplicationDbContext dbContext,
        IDataShapeHelper<Employee> dataShaper,
        IMockService mockData) : base(dbContext)
    {
        _dataShaper = dataShaper;
        _mockData = mockData;
    }

    public async Task<(IEnumerable<Entity> data, RecordsCount recordsCount)> GetPagedEmployeeReponseAsync(
        GetEmployeesQuery requestParameter)
    {
        IQueryable<Employee> result;

        var employeeNumber = requestParameter.EmployeeNumber;
        var employeeTitle = requestParameter.EmployeeTitle;

        var pageNumber = requestParameter.PageNumber;
        var pageSize = requestParameter.PageSize;
        var orderBy = requestParameter.OrderBy;
        var fields = requestParameter.Fields;

        int recordsTotal, recordsFiltered;

        var seedCount = 1000;

        result = _mockData.GetEmployees(seedCount)
            .AsQueryable();

        // Count records total
        recordsTotal = result.Count();

        // filter data
        FilterByColumn(ref result, employeeNumber, employeeTitle);

        // Count records after filter
        recordsFiltered = result.Count();

        //set Record counts
        var recordsCount = new RecordsCount
        {
            RecordsFiltered = recordsFiltered,
            RecordsTotal = recordsTotal
        };

        // set order by
        if (!string.IsNullOrWhiteSpace(orderBy)) result = result.OrderBy(orderBy);

        //limit query fields
        if (!string.IsNullOrWhiteSpace(fields)) result = result.Select<Employee>("new(" + fields + ")");
        // paging
        result = result
            .Skip((pageNumber - 1) * pageSize)
            .Take(pageSize);

        // retrieve data to list
        // var resultData = await result.ToListAsync();
        // Note: Bogus library does not support await for AsQueryable.
        // Workaround:  fake await with Task.Run and use regular ToList
        var resultData = await Task.Run(() => result.ToList());

        // shape data
        var shapeData = _dataShaper.ShapeData(resultData, fields);

        return (shapeData, recordsCount);
    }

    private void FilterByColumn(ref IQueryable<Employee> positions, string employeeNumber, string employeeTitle)
    {
        if (!positions.Any())
            return;

        if (string.IsNullOrEmpty(employeeTitle) && string.IsNullOrEmpty(employeeNumber))
            return;

        var predicate = PredicateBuilder.New<Employee>();

        if (!string.IsNullOrEmpty(employeeNumber))
            predicate = predicate.And(p => p.EmployeeNumber.Contains(employeeNumber.Trim()));

        if (!string.IsNullOrEmpty(employeeTitle))
            predicate = predicate.And(p => p.EmployeeTitle.Contains(employeeTitle.Trim()));

        positions = positions.Where(predicate);
    }
}