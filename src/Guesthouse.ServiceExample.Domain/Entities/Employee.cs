﻿using System;
using Guesthouse.Library.Common.Core.Domain;
using Guesthouse.ServiceExample.Domain.Enums;

namespace Guesthouse.ServiceExample.Domain.Entities;

public class Employee : EntityBase
{
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
    public string LastName { get; set; }
    public string EmployeeTitle { get; set; }
    public DateTime DOB { get; set; }
    public string Email { get; set; }
    public Gender Gender { get; set; }
    public string EmployeeNumber { get; set; }
    public string Suffix { get; set; }
    public string Phone { get; set; }
}