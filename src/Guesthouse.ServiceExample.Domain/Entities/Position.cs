﻿using Guesthouse.Library.Common.Core.Domain;

namespace Guesthouse.ServiceExample.Domain.Entities;

public class Position : AuditableEntityBase
{
    public string PositionTitle { get; set; }
    public string PositionNumber { get; set; }
    public string PositionDescription { get; set; }
    public string PostionArea { get; set; }
    public string PostionType { get; set; }
    public decimal PositionSalary { get; set; }
}