﻿namespace Guesthouse.ServiceExample.Domain.Enums;

public enum Gender
{
    Male,
    Female
}