﻿using System.Threading.Tasks;
using Guesthouse.Library.Common.Presentation.Controllers;
using Guesthouse.ServiceExample.Application.Features.Employees.Queries.GetEmployees;
using Guesthouse.ServiceExample.WebApi.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Guesthouse.ServiceExample.WebApi.Controllers.v1;

[ApiVersion("1.0")]
public class EmployeesController : ApiControllerBase
{
    /// <summary>
    ///     GET: api/controller
    /// </summary>
    /// <param name="filter"></param>
    /// <returns></returns>
    [HttpGet]
    [Authorize(Policy = Policies.EmployeesRead)]
    public async Task<IActionResult> Get([FromQuery] GetEmployeesQuery filter)
    {
        return Ok(await Mediator.Send(filter));
    }
}