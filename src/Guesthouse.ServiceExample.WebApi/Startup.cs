using Guesthouse.Library.Authorization.Extensions;
using Guesthouse.Library.Common.Presentation.Extensions;
using Guesthouse.Library.HealthChecks.Extensions;
using Guesthouse.ServiceExample.Application;
using Guesthouse.ServiceExample.Infrastructure.Persistence;
using Guesthouse.ServiceExample.Infrastructure.Persistence.Contexts;
using Guesthouse.ServiceExample.Infrastructure.Shared;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Guesthouse.ServiceExample.WebApi;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        _config = configuration;
    }

    public IConfiguration _config { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddApplicationLayer();
        services.AddPersistenceInfrastructure(_config);
        services.AddSharedInfrastructure(_config);
        services.AddSwaggerExtension(_config);
        services.AddControllersExtension();
        services.AddCorsExtension();
        services.AddJwtAuthentication(_config);
        services.AddAuthorizationPolicies(_config);
        services.AddApiVersioningExtension();
        services.AddMvcCore().AddApiExplorer();
        services.AddVersionedApiExplorerExtension();
        services.AddHealthChecks(_config);
    }

    public void Configure(
        IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory,
        ApplicationDbContext dbContext)
    {
        app.UseServiceBasePath(env, _config);
        
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Error");
            app.UseHsts();
        }

        dbContext.Database.EnsureCreated();

        app.UseHttpsRedirection();
        app.UseRouting();
        app.UseCors("AllowAll");
        app.UseAuthentication();
        app.UseErrorHandlingMiddleware();
        app.UseAuthorization();
        app.UseSwaggerExtension(env, _config);

        app.UseEndpoints(endpoints => { endpoints.MapControllers(); })
            .UseHealthCheckEndpoint(_config);
    }
}