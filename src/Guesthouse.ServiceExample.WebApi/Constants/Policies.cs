﻿namespace Guesthouse.ServiceExample.WebApi.Constants;

public static class Policies
{
    public const string EmployeesRead = "employees.read";
    public const string EmployeesWrite = "eployees.write";
    public const string PositionsRead = "positions.read";
    public const string PositionsWrite = "positions.write";
}