﻿using Guesthouse.Library.HealthChecks.HealthChecks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Guesthouse.ServiceExample.WebApi;

public static class HealthCheck
{
    public static IServiceCollection AddHealthChecks(this IServiceCollection services, IConfiguration config)
    {
        var connectionString = config.GetConnectionString("DefaultConnection");
        var dbName = SqlConnectionStringParser.GetDatabaseName(connectionString);
        const string dbHealthQuery = "SELECT TOP 1 * FROM dbo.Positions";

        services
            .AddHealthChecks()
            .AddSqlServer(
                connectionString,
                dbHealthQuery,
                dbName,
                tags: new[] { TagNames.Database })
            .AddBasicChecks(services, config);

        return services;
    }
}